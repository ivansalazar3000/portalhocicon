## Instalacion del instalacion del Backend

## Requisitos Previos

Asegúrate de tener instalados los siguientes requisitos en tu máquina:

- Node.js: [Descargar e Instalar Node.js](https://nodejs.org/)
- npm: (Normalmente se instala automáticamente con Node.js)
- postgres v15: Base de datos utilizado en el proyecto

## Instalacion del CLI de nest

Para instalar la CLI de Nest.js globalmente en tu sistema, puedes utilizar npm:

- npm install -g @nestjs/cli

## Instrucciones de Instalación

A continuación, se detallan los pasos para clonar, configurar y ejecutar el proyecto de Nest.js en tu entorno local.

## clonar repositorio

- git clone https://gitlab.com/ivansalazar3000/portalhocicon.git

## Instalar dependencia
Ejecutar en a terminal:
- npm install


## Configuración del Entorno
Crear: un archivo .env en la raíz del proyecto y configura las variables de entorno necesarias.

- PORT=3001
- DATABASE_URL=portal-noticias

## crear BD en postgrest
-
- nombreBD: portal-noticias

## crear seedes
Ejecutar en la terminal:
- npm run seed:run


## ejecutar proyecto
Modo de Desarrollo: Para ejecutar en modo de desarrollo con reinicio automático:
- npm run start:dev







