import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import { ConfigService } from '@nestjs/config';

async function bootstrap() {
  const app = await NestFactory.create(AppModule);
  const configService = app.get(ConfigService);
  const host = configService.get('HOST', 'localhost');
  const port = configService.get('PORT', 3001);
  app.setGlobalPrefix('portal');
  app.enableCors();
  await app.listen(port, host, () =>{
  console.log(`Servidor ejecutando en http://${host}:${port}`);   
  });
}
bootstrap();
