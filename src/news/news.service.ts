import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { New } from './entities/new.entity';


@Injectable()
export class NewsService {
  constructor(
    @InjectRepository(New)
    private newRepository: Repository<New>,
  ) {}

  async findAll(): Promise<New[]> {
    return this.newRepository.find();
  }

  async findOne(id: number): Promise<New> {
    return this.newRepository.findOne({ where: { id } });
  }

  async create(news: Partial<New>): Promise<New> {
    const newnew = this.newRepository.create(news);
    return this.newRepository.save(newnew);
  }

  async update(id: number, news: Partial<New>): Promise<New> {
    await this.newRepository.update(id, news);
    return this.newRepository.findOne({ where: { id } });
  }

  async delete(id: number): Promise<void> {
    await this.newRepository.delete(id);
  }
}





