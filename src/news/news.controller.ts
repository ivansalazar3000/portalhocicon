

import { Controller, Get, Post, Body, Put, Param, Delete, NotFoundException } from '@nestjs/common';
import { NewsService } from './news.service';
import { New } from './entities/new.entity';


@Controller('news')
export class NewsController {
  constructor(private readonly newsService: NewsService) {}

@Get()
async findAll(): Promise<New[]> {
  return this.newsService.findAll();
}

@Get(':id')
  async findOne(@Param('id') id: number): Promise<New> {
    const noti = await this.newsService.findOne(id);
    if (!noti) {
      throw new NotFoundException('Noticia no encontrada!');
    } else {
      return noti;
    }
  }

  //create noticia
  @Post()
  async create(@Body() noti: New): Promise<New> {
    return this.newsService.create(noti);
  }


  //update noticia
  @Put(':id')
  async update (@Param('id') id: number, @Body() noti: New): Promise<any> {
    return this.newsService.update(id, noti);
  }

   //delete noticia
   @Delete(':id')
   async delete(@Param('id') id: number): Promise<any> {
     //handle error if user does not exist
     const noti = await this.newsService.findOne(id);
     if (!noti) {
       throw new NotFoundException('Noticia no encontrada!');
     }
     return this.newsService.delete(id);
   }


}






