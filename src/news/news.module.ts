import { Module } from '@nestjs/common';
import { NewsService } from './news.service';
import { NewsController } from './news.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { New } from './entities/new.entity';

@Module({
  imports:[
    TypeOrmModule.forFeature([New])],
    controllers: [NewsController],
    providers: [NewsService]
})
export class NewsModule {}
