
import { Entity, PrimaryGeneratedColumn, Column, CreateDateColumn, UpdateDateColumn, DeleteDateColumn } from "typeorm";

@Entity('news')
export class New { 

    @PrimaryGeneratedColumn('increment',{ name:'id', type:'int' })
    id: number;

    @Column({ name: 'tituloPublicacion', type: 'varchar', length: 100, nullable: true})
    tituloPublicacion: string;

    @Column({ name: 'fechaPublicacion',type: 'date', nullable: true })
    fechaPublicacion: Date;

    @Column({ name: 'lugarPublicacion', type: 'varchar', length: 100, nullable: true})
    lugarPublicacion: string;

    @Column({ name: 'contenidoPublicacion', type: 'varchar', length: 250, nullable: true})
    contenidoPublicacion: string;

    @CreateDateColumn({ name: 'fechaCreacion', type: 'timestamp', default: ()=>'CURRENT_TIMESTAMP(6)' })
    fechaCreacion: Date; 

    @UpdateDateColumn({ name: 'actualizadoEn', type: 'timestamp', default: ()=>'CURRENT_TIMESTAMP(6)' })
    actualizadoEn: Date;
    
    @DeleteDateColumn({ name: 'eliminadoEn', type: 'timestamp', nullable: true })
    eliminadoEn: Date;

}