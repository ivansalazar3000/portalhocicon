
import { Seeder, SeederFactoryManager } from 'typeorm-extension';
import { DataSource } from 'typeorm';
import { New } from '../../news/entities/new.entity';

export class NewSeeder implements Seeder {
  async run(
    dataSource: DataSource,
    factoryManager: SeederFactoryManager
    ): Promise<void> {

  
    const newRepository = dataSource.getRepository(New);

    const newData =
       [{
      tituloPublicacion: '2024 con nuevos desafios...',
      fechaPublicacion: '12-01-2024',
      lugarPublicacion:'La paz',
      contenidoPublicacion:'Este 2024 varias personas buscan un lugar para realizar pasantias en la AGETIC, espero ser una de las personas elegidas',
      },{
      tituloPublicacion: 'Nest js',
      fechaPublicacion: '13-01-2024',
      lugarPublicacion:'La paz',
      contenidoPublicacion:'Nest es un marco de desarrollo para la construcción de aplicaciones del lado del servidor (backend) en Node.js.',
      },
      {
        tituloPublicacion: 'Next js',
        fechaPublicacion: '13-01-2024',
        lugarPublicacion:'La paz',
        contenidoPublicacion:'Next.js es un marco de desarrollo para React que facilita la creación de aplicaciones web React de manera eficiente y estructurada.',
        },
        {
          tituloPublicacion: 'React js',
          fechaPublicacion: '13-01-2024',
          lugarPublicacion:'La paz',
          contenidoPublicacion:'React.js (también conocido simplemente como React) es una biblioteca de JavaScript desarrollada y mantenida por Facebook. Se utiliza para construir interfaces de usuario interactivas y reactivas para aplicaciones web.',
          }];

    const newNew = newRepository.create(newData);
    await newRepository.save(newNew); 

  }
}
