
import 'reflect-metadata'
import { DataSource, DataSourceOptions } from 'typeorm';
import { SeederOptions, runSeeder, runSeeders } from 'typeorm-extension';
import { MainSeeder } from './MainSeeder';





const options: DataSourceOptions & SeederOptions = {
    type:'postgres',
    host:'localhost',
    port: 5432,
    username:'postgres',
    password:'postgre',
    database: 'portal-noticias',
    synchronize: true,
    entities: [__dirname + '/../**/*.entity.{js,ts}'],
    seeds:[MainSeeder]
};

export const AppDataSource = new DataSource(options);

        /*
        AppDataSource.initialize().then(async ()=>{
            await AppDataSource.synchronize(true);
            await runSeeders(AppDataSource);
            process.exit();
        
        })
        */