import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { ConfigModule } from '@nestjs/config';
import { NewsModule } from './news/news.module';
import { TypeOrmModule } from '@nestjs/typeorm';
import { New } from './news/entities/new.entity';

@Module({
  imports: [
    ConfigModule.forRoot(), 
    NewsModule, 
    TypeOrmModule.forRoot({
    type:'postgres',
    host:'localhost',
    port: 5432,
    username:'postgres',
    password:'postgre',
    database: 'portal-noticias',
    entities: [New],
    synchronize: true,
    autoLoadEntities: true  
  })],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
